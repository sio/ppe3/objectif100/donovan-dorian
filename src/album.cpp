#include "../header/album.h"
#include <iostream>
#include <chrono>

using namespace std;

album::album():
  id(0),
  name("")
{
  date.std::chrono::system_clock::now();
}

album::album(int _id, string _name, chrono::system_clock _date):
  id(_id),
  name(_name),
  date(_date)
{}

int album::getId()
{
  return id;
}

string album::getName()
{
  return name;
}

void album::setName(string _name)
{
  name = _name;
}

chrono::system_clock album::getDate()
{
  return date;
}

void album::setDate(chrono::system_clock _date)
{
  date = _date;
}
