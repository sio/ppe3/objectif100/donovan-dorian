#include "../header/track.h"
#include <iostream>
#include <vector>

using namespace std;

track::track():
  id(0),
  name(""),
  path(""),
  duration(0),
  Format(),
  Genre(),
  Subgenre(),
  Polyphony()
{}

track::track(int _id, string _name, string _path, int _duration, vector<artist> _artists, vector<album> _albums, format _format, genre _genre, subgenre _subgenre, polyphony _polyphony):
  id(_id),
  name(_name),
  path(_path),
  duration(_duration),
  artists(_artists),
  albums(_albums),
  Format(_format),
  Genre(_genre),
  Subgenre(_subgenre),
  Polyphony(_polyphony)
{}

int track::getId()
{
  return id;
}

int track::getDuration()
{
  return id;
}

void track::setDuration(int _duration)
{
  duration = _duration;
}

string track::getName()
{
  return name;
}

void track::setName(string _name)
{
  name = _name;
}

string track::getPath()
{
  return path;
}

void track::setPath(string _path)
{
  path = _path;
}

format track::getFormat()
{
  return Format;
}

void track::setFormat(format _Format)
{
  Format = _Format;
}

genre track::getGenre()
{
  return Genre;
}

void track::setGenre(genre _Genre)
{
  Genre = _Genre;
}

subgenre track::getSubgenre()
{
  return Subgenre;
}

void track::setSubgenre(subgenre _Subgenre)
{
  Subgenre = _Subgenre;
}

polyphony track::getPolyphony()
{
  return Polyphony;
}

void track::setPolyphony(polyphony _Polyphony)
{
  Polyphony = _Polyphony;
}
