#include "../header/playlist.h"
#include "../header/cli.h"
#include <libpq-fe.h>
#include <iostream>

using namespace std;
int main(int argc, char *argv[])
{
  int code_retour = 0;
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  std::cout << FLAGS_genre << std::endl;
  std::cout << FLAGS_duration << std::endl;
  std::cout << FLAGS_playlist << std::endl;
  vector<track> vector_track;
  playlist p(1, FLAGS_playlist, vector_track, FLAGS_duration);
  p.writeM3U();
  char info_connexion[] = "host=postgresql.bts-malraux72.net port=5432 user=d.verrier dbname=Cours password=P@ssword";
  PGPing ping = PQping(info_connexion);

  if(ping == PQPING_OK)
  {
    cout << "Serveur accessible" << endl;
    PGconn *connexion = PQconnectdb(info_connexion);

    if(PQstatus(connexion) == CONNECTION_OK)
    {
      int durationTotal = 0;
      cout << "Connexion établie" << endl;
      const char requete[] = "SET SCHEMA 'radio_libre';SELECT \"morceau\".duree, \"artiste\".nom, \"morceau\".nom, \"morceau\".chemin from \"morceau\" INNER JOIN \"artiste_morceau\" ON \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"artiste\" ON \"artiste_morceau\".id_artiste = \"artiste\".id;";
      PGresult *exec = PQexec(connexion, requete);
      ExecStatusType resultat = PQresultStatus(exec);

      if(resultat == PGRES_TUPLES_OK)
      {
        cout << "Le résultat de la requete fonctionne" << endl;
        code_retour = 0;
      }
      else
      {
        cerr << "Le résultat de la requete ne fonctionne pas" << endl;
      }
    }
    else
    {
      cerr << "Connexion non établie" << endl;
    }
  }
  else
  {
    cerr << "Serveur non accessible" << endl;
  }

  vector<track> manyTracks;
  playlist pp(1, "Coucou", manyTracks, 200);
  return code_retour;
}
