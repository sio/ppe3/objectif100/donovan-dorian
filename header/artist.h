#ifndef ARTIST_H
#define ARTIST_H
#include <iostream>
using namespace std;

/**
 * \class artist
 * \brief definition of the class artist
 */
class artist
{
 public:

  /**
   * \brief default constructor of artist
   */
  artist();

  /**
   * \brief constructor with parameters of artist
   * \param id the identifier of an artist
   * \param name the name of an artist
   */
  artist(int, string);

  /**
   * \brief allows reading an identifier of an artist
   * \return int representing the id of an artist
   */
  int getId();

  /**
   * \brief allows reading a name of an artist
   * \return string representing the name of an artist
   */
  string getName();

  /**
   * \brief allows writting a name of an artist
   */
  void setName(string);

 private:
  int id;///< Attribute defining the id of an artist
  string name;///< Attribute defining the name of an artist
};

#endif // ARTIST_H
