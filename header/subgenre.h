#ifndef SUBGENRE_H
#define SUBGENRE_H
#include <iostream>
using namespace std;

/**
 * \class subgenre
 * \brief definition of the class subgenre
 */
class subgenre
{
 public:

  /**
   * \brief default constructor of subgenre
   */
  subgenre();

  /**
   * \brief constructor with parameters of subgenre
   * \param id the identifier of a subgenre
   * \param subtype the subtype of a subgenre
   */
  subgenre(int, string);

  /**
   * \brief allows reading an identifier of a subgenre
   * \return int representing the id of a subgenre
   */
  int getId();

  /**
   * \brief allows reading a subtype of a subgenre
   * \return string representing the subtype of a subgenre
   */
  string getSub_type();

  /**
   * \brief allows writting a subtype of a subgenre
   */
  void setSub_type(string);

 private:
  int id;///< Attribute defining the id of a subgenre
  string sub_type;///< Attribute defining the subtype of a subgenre
};
#endif // SUBGENRE_H
