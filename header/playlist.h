#ifndef PLAYLIST_H
#define PLAYLIST_H
#include <iostream>
#include <vector>
#include "track.h"

using namespace std;

/**
 * \class playlist
 * \brief Definition of the class playlist
 */
class playlist
{
 public:
  /**
   * \brief default constructor of playlist
   */
  playlist();
  /**
   * \brief constructor with parameters of playlist
   * \param id the identifier of a playlist
   * \param title the title of a playlist
   * \param vector<track> the list of track for a playlist
   * \param duration the duration of a playlist
   */
  playlist(int, string, vector<track>, int);

  /**
   * \brief allows reading a identifier of a playlist
   * \return int representing the id of a playlist
   */
  int getId();

  /**
   * \brief allows reading a title of a playlist
   * \return String representing the title of a playlist
   */
  string getTitle();

  /**
   * \brief to set the title of a playlist
   */
  void setTitle(string);

  /**
   * \brief allows reading a duration of a playlist
   * \return int representing the duration of a playlist
   */
  int getDuration();

  /**
   * \brief to set the duration of a playlist
   */
  void setDuration(int);

  /**
   * \brief to write the playlist in M3U file format
   */
  void writeM3U();

  /**
   * \brief to write the playlist in XSPF file format
   */
  void writeXSPF();

 private:
  int id;///< Attribute defining the id of a playlist
  string title;///< Attribute defining the title of a playlist
  vector<track> Tracks;///< Attribute defining tracks of a playlist
  int duration;///< Attribute defining the duration of a playlist
};
#endif // PLAYLIST_H
