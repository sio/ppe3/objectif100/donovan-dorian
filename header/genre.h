#ifndef GENRE_H
#define GENRE_H
#include <iostream>
using namespace std;

/**
 * \class genre
 * \brief definition of the class genre
 */
class genre
{
 public:

  /**
   * \brief default constructor of genre
   */
  genre();

  /**
   * \brief constructor with parameters of genre
   * \param id the identifier of a genre
   * \param type the type of a genre
   */
  genre(int, string);

  /**
   * \brief allows reading an identifier of a genre
   * \return int representing the id of a genre
   */
  int getId();

  /**
   * \brief allows reading a type of a genre
   * \return string representing the type of a genre
   */
  string getType();

  /**
   * \brief allows writting a type of a genre
   */
  void setType(string);

 private:
  int id;///< Attribute defining the id of a genre
  string type;///< Attribute defining the type of a genre
};

#endif // GENRE_H
